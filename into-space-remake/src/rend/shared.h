#ifndef _INTO_SPACE_REMAKE_RENDER_SHARED_H_
#define _INTO_SPACE_REMAKE_RENDER_SHARED_H_

#include <glad/glad.h>

namespace rend {
	constexpr GLint itemsTexturePos = 0;
	constexpr GLint rectanglesTexturePos = 1;
}

#endif